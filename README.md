# Back It Up

Using the dropbox API with python to backup my folders (and also restore them).

This is a command line tool made with __click__ library to ease the process of making backup of
my system.



Examples:

```
$ python app.py backup tmp/ --ignore
$ python app.py restore "/Backup" "/home/sjukdom/backup.zip"
```



## TODO:
- [ ] Use a crontab job to backup a list of folders periodically