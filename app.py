import sys
import click
from pathlib import Path
from helpers.service import back_it_up, restore_it


@click.group()
def cli():
    pass


@cli.command()
@click.argument("directories", nargs=-1)
@click.option("--ignore", "-i", is_flag=True)
def backup(directories, ignore):
    """
        Backup the full directory specified.
        You can add files and directories in .ignorefiles
        and use the flag --ignore or -i
    """
    click.secho("Backup directory: {}".format(", ".join(directories)), fg="green")
    ignorefiles = None

    for directory in directories:
        if not Path.is_dir(Path(directory)):
            sys.exit("Aborting: make sure the directory exist")


    if ignore:
        if not Path(".ignorefiles").is_file():
            click.secho("Aborting: the .ignorefiles doesn't exist", fg="red")
            raise Exception()
        click.secho("Ignoring files and directories at .ignorefiles", fg="cyan")
        with open(".ignorefiles", "r") as file:
            ignorefiles = [ file_item.strip() for file_item in file.readlines() ]
    
    back_it_up(directories, backup_path="/Backup", ignorefiles=ignorefiles)


@cli.command()
@click.argument("dbx_path", nargs=1)
@click.argument("download_path", nargs=1)
def restore(dbx_path, download_path):
    """
        Restore the directory from Dropbox into the 
        specified file path (must end with .zip extension)
    """
    if not download_path.endswith(".zip"):
        click.secho("Error: the file path {} must have the .zip extension".format(download_path), fg="red")
        sys.exit()
    click.secho("Restoring the directory {} into {}".format(dbx_path, download_path), fg="green")
    restore_it(dbx_path=dbx_path, download_path=download_path)
    


if __name__ == "__main__":
    cli()
