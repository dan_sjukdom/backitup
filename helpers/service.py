import os
import re
import sys
import dropbox
from dropbox.exceptions import ApiError, AuthError
from dropbox.files import WriteMode
from dotenv import load_dotenv
import logging


logger = logging.getLogger(__name__)
logger.setLevel("INFO")
file_handler = logging.FileHandler("service.log")
file_handler.setLevel("INFO")
logger.addHandler(file_handler)


load_dotenv()
TOKEN = os.getenv("DBOX_TOKEN", "")

BASE_PATH = os.getcwd()



def restore_it(dbx_path, download_path):
    """
        Download the target Dropbox folder to the provided
        download path
    """
    with dropbox.Dropbox(TOKEN) as dbx:
        try:
            dbx.files_download_zip_to_file(download_path, dbx_path)
        except ApiError:
            logger.error("The specified folder doesn't exist in Dropbox")


def back_it_up(directories, backup_path="/Backup", ignorefiles=None):
    """
        Upload the set of directories inside a folder at the root called /Backup.
        If there are ignorefiles then don't upload them nor its content
    """
    with dropbox.Dropbox(TOKEN) as dbx:

        try:
            dbx.files_get_metadata(backup_path)
        except ApiError:
            logger.warning("The folder {} doesn't exist, creating it".format(backup_path))
            dbx.files_create_folder(backup_path)
        else:
            logger.info("The backup folder {} already exists".format(backup_path))

        for directory in directories:

            directory = directory.strip("/")
            
            for root, dirs, files in os.walk(directory):
                dbx_path = backup_path + "/" + root[root.find(os.path.basename(directory)):]
                current_dir = os.path.basename(dbx_path)
                if ignorefiles:
                    if current_dir in ignorefiles:
                        logger.info("Ignoring the directory {current_dir}".format(current_dir))
                        continue
                    elif any([ parent_dir in ignorefiles for parent_dir in dbx_path.split("/")]):
                        continue

                for _file in files:
                    dbx_file_path = dbx_path + "/" + _file
                    system_file_path = dbx_path.replace("/Backup", BASE_PATH) + "/" + _file
                    if ignorefiles:
                        if _file in ignorefiles:
                            continue
                    logger.info("Uploading the file {} to {}".format(_file, dbx_file_path))
                    with open(system_file_path, "rb") as f:
                        dbx.files_upload(f.read(), dbx_file_path, mode=WriteMode("overwrite"))