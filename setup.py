from setuptools import setup, find_packages


setup(
    description="A backup tool used to upload/download files from Google Drive and Dropbox",
    version="1.0.0",
    packages=find_packages(),
)
